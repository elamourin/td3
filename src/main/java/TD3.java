/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture, en base 10, de n
 */
int nbChiffres(int n)
{
    int nbrunite = 1;
    while (n > 10)
    {
        n = n / 10;
        nbrunite++;
    }
    return nbrunite;
}

/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture du nombre n^2
 */
int nbChiffresDuCarre(int n)
{
    int ncarre = n * n;
    int nbrunite = 1;
    while (ncarre / 10 > 0)
    {
        ncarre = ncarre / 10;
        nbrunite++;
    }
    return nbrunite;
}

void testnbdeChiffre()
{
    Ut.afficherSL("saisir le nombre");
    int nombre = Ut.saisirEntier();
    Ut.afficherSL("le nombre a " + nbChiffresDuCarre(nombre) + " uniter");
}

int max2(int max, int b)
{
    if (max < b)
    {
        max = b;
    }
    return max;
}

int max3(int max, int b, int c)
{
    int maxmax = 0;
    if (max2(max, b) > c)
    {
        maxmax = max2(max, b);
    }
    else
    {
        maxmax = c;
    }
    return maxmax;
}

void testMax()
{
    Ut.afficherSL("saisir entier");
    int x = Ut.saisirEntier();
    Ut.afficherSL("saisir entier");
    int y = Ut.saisirEntier();
    Ut.afficherSL("saisir entier");
    int z = Ut.saisirEntier();
    int maxbis = max3(x, y, z);
    Ut.afficherSL("le maximum entre " + x + "," + y + " et " + z + " est " + maxbis);
}

String repeteCarac(int nb, char car)
{
    int i = 0;
    String chaine = "";
    while (i < nb)
    {
        chaine = chaine + car;
        i++;
    }
    Ut.afficher(chaine);
    return chaine;
}

String pyramideSimple(int h, char c)
{
    String pyramide = "";
    int i = 0;
    while (i < h)
    {
        int n = (i * 2) + 1;
        int espaces = h - i - 1;
        String espacecarac = "";
        int a = 0;
        while (a < espaces)
        {
            espacecarac += " ";
            a++;
        }
        i++;
    }
    return pyramide;
}

void testPyramideSimple()
{
    Ut.afficher("Saisir la hauteur de la pyramide : ");
    int hauteur = Ut.saisirEntier();
    Ut.afficher("Saisir le caractère de la pyramide : ");
    char caractere = Ut.saisirCaractere();
    String pyra = pyramideSimple(hauteur, caractere);
    Ut.afficher(pyra);
}

void afficheNombresCroissants(int nb1, int nb2)
{
    while (nb1 <= nb2)
    {
        Ut.afficher(nb1 % 10);
        nb1++;
    }
}

void afficheNombresDecroissants(int nb1, int nb2)
{
    while (nb1 <= nb2)
    {
        Ut.afficher(nb2 % 10);
        nb2--;
    }
}

void pyramideElaboree(int h)
{
    char a = ' ';
    int i = 1;
    int espaces = h - 1;
    while (i < h)
    {
        repeteCarac(espaces, a);
        espaces--;
        afficheNombresCroissants(i, i * 2 - 1);
        afficheNombresDecroissants(i, i * 2 - 2);
        Ut.sautLigne();
        i++;
    }
}

void testpyramideElaboree()
{
    Ut.afficher("saisir la hauteur de la pyramide : ");
    int hauteur = Ut.saisirEntier();
    pyramideElaboree(hauteur);
}

/**
 * @param c
 * @return la racine carrée entière n d'un nombre entier c donné,
 * si c est un carré parfait, c'est-à-dire si c = n * n.
 * Sinon la fonction retourne -1.
 */
int racineParfaite(int c)
{
    int n = -1;
    int i = 0;
    while (i < c)
    {
        if (i * i == c)
        {
            n = i;
        }
        i++;
    }
    return n;
}

/**
 *
 * @param nb
 * @return vrai si un entier donné est un carré parfait, faux sinon.
 */
boolean estCarreParfait(int nb)
{
    return racineParfaite(nb) != -1;
}

/**
 *
 * @param p
 * @param q
 * @return vrai si deux nombres entiers `p` et `q` sont amis, faux sinon.
 */
boolean nombresAmicaux(int p, int q)
{
    int pn = 1;
    int qn = 1;
    int qami = 0;
    int pami = 0;
    while (pn < p)
    {
        if (p % pn == 0)
        {
            pami += pn;
        }
        pn++;
    }
    while (qn < q)
    {
        if (q % qn == 0)
        {
            qami += qn;
        }
        qn++;
    }
    return pami == q && qami == p;
}

/**
 * Affiche à l'écran les couples de nombres amis parmi les nombres inférieurs ou égaux à max.
 * @param max
 */
void afficheAmicaux(int max)
{
    int i1 = 1;
    int i2 = 1;
    while (i2 <= max)
    {
        while (i1 <= max)
        {
            if (nombresAmicaux(i1, i2))
            {
                Ut.afficher(i1 + "," + i2 + ":");
            }
            i1++;
        }
        i2++;
        i1 = i2 + 1;
    }
}

/**
 *
 * @param c1
 * @param c2
 * @return vrai si deux entiers donnés peuvent être les côtés de l'angle droit d'un triangle rectangle dont les trois côtés sont des nombres entiers, faux sinon
 */
boolean estTriangleRectangle(int c1, int c2)
{
    int c3 = c1 * c1 + c2 * c2;
    return estCarreParfait(c3);
}

int nbrDeTriangleRectangle(int a)
{
    int i = 0;
    int i1 = 1;
    int i2 = 1;
    double i3 = 1;
    while (i2 + i1 + i3 <= a)
    {
        while (i1 + i2 + i3 <= a)
        {
            if (estTriangleRectangle(i1, i2))
            {
                i++;
            }
            i1++;
            i3 = Math.sqrt(i1 * i1 + i2 * i2);
        }
        i2++;
        i1 = i2 + 1;
        i3 = Math.sqrt(i1 * i1 + i2 * i2);
    }
    return i;
}

// n et nbMaxOp sont des entiers positifs
boolean syracusiens(int n, int nbMaxOp)
{
    boolean reponse = false;
    int i = 0;
    while (i < nbMaxOp)
    {
        if (n % 2 == 0)
        {
            n = n / 2;
        }
        else
        {
            n = 3 * n + 1;
        }
        i++;
        if (n == 1)
        {
            reponse = true;
            break;
        }
    }
    return reponse;
}

void main()
{
    Ut.afficherSL("saisir le nombre à vérifier");
    int b = Ut.saisirEntier();
    Ut.afficherSL("saisir le nombre d'itérations");
    int nb = Ut.saisirEntier();
    Ut.afficherSL(syracusiens(b, nb));
}
